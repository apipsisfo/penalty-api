﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_Alloc")]
    public partial class LK_Alloc
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [Required]
        [StringLength(3)]
        public string allocCode { get; set; }

        [Required]
        [StringLength(30)]
        public string allocDesc { get; set; }

        public bool isVAT { get; set; }

        public bool isActive { get; set; }

        [ForeignKey("LK_PayFor")]
        public int? payForID { get; set; }
        public virtual LK_PayFor LK_PayFor { get; set; }
    }
}
