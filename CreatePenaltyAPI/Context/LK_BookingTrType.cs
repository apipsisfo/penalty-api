﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_BookingTrType")]
    public partial class LK_BookingTrType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(2)]
        public string bookingTrType { get; set; }

        [Required]
        [StringLength(40)]
        public string ket { get; set; }
    }
}
