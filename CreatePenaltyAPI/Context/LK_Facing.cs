﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_Facing")]
    public partial class LK_Facing
    {
        [Key]
        public int Id { get; set; }
        //unique
        [Required]
        [StringLength(3)]
        public string facingCode { get; set; }

        [Required]
        [StringLength(50)]
        public string facingName { get; set; }        
    }
}
