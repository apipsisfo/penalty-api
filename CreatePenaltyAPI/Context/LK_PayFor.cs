﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_PayFor")]
    public partial class LK_PayFor
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(3)]
        public string payForCode { get; set; }

        [Required]
        [StringLength(40)]
        public string payForName { get; set; }

        public bool isSched { get; set; }

        public bool isIncome { get; set; }

        public bool isInventory { get; set; }

        public bool isSDH { get; set; }

        public bool isActive { get; set; }
    }
}
