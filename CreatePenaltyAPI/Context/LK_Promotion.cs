﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_Promotion")]
    public class LK_Promotion
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(3)]
        public string promotionCode { get; set; }

        [Required]
        [StringLength(100)]
        public string promotionDesc { get; set; }   
    }
}
