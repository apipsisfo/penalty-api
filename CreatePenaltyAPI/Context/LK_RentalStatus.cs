﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_RentalStatus")]
    public partial class LK_RentalStatus
    {
        [Key]
        public int Id { get; set; }

        //unique
        [Required]
        [StringLength(1)]
        public string rentalStatusCode { get; set; }

        [Required]
        [StringLength(50)]
        public string rentalStatusName { get; set; }

        public ICollection<MS_Unit> MS_Unit { get; set; }
    }
}
