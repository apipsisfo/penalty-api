﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_SADStatus")]
    public class LK_SADStatus
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(1)]
        public string statusCode { get; set; }

        [Required]
        [StringLength(50)]
        public string statusDesc { get; set; }   
    }
}
