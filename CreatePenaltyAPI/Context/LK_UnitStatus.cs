﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("LK_UnitStatus")]
    public partial class LK_UnitStatus
    {
        [Key]
        public int Id { get; set; }

        //unique
        [Required]
        [StringLength(1)]
        public string unitStatusCode { get; set; }

        [Required]
        [StringLength(50)]
        public string unitStatusName { get; set; }        
    }
}
