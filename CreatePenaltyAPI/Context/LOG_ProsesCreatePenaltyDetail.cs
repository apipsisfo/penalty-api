﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("LOG_ProsesCreatePenaltyDetail")]
    public class LOG_ProsesCreatePenaltyDetail
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("LOG_ProsesCreatePenaltyHeader")]
        public int HeaderId { get; set; }
        public virtual LOG_ProsesCreatePenaltyHeader LOG_ProsesCreatePenaltyHeader { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Bookcode { get; set; }
        public string FailMessage { get; set; }
    }
}
