﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("LOG_ProsesCreatePenaltyHeader")]
    public class LOG_ProsesCreatePenaltyHeader
    {
        [Key]
        public int Id { get; set; }
        public int Succes { get; set; }
        public int Fail { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
