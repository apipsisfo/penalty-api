﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Area")]
    public partial class MS_Area
    {
        [Key]
        public int Id { get; set; }

        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string areaCode { get; set; }

        [Required]
        [StringLength(30)]
        public string regionName { get; set; }

        [ForeignKey("MS_City")]
        public int cityID { get; set; }
        public virtual MS_City MS_City { get; set; }
    }
}
