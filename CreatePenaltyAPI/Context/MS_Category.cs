﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Category")]
    public partial class MS_Category
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(5)]
        public string categoryCode { get; set; }

        [Required]
        [StringLength(30)]
        public string categoryName { get; set; }

        [Required]
        [StringLength(30)]
        public string projectField { get; set; }

        [Required]
        [StringLength(30)]
        public string areaField { get; set; }

        [Required]
        [StringLength(30)]
        public string categoryField { get; set; }

        [Required]
        [StringLength(30)]
        public string clusterField { get; set; }

        [Required]
        [StringLength(30)]
        public string productField { get; set; }

        [Required]
        [StringLength(30)]
        public string detailField { get; set; }

        [Required]
        [StringLength(30)]
        public string zoningField { get; set; }

        [Required]
        [StringLength(30)]
        public string facingField { get; set; }

        [Required]
        [StringLength(30)]
        public string roadField { get; set; }

        [Required]
        [StringLength(30)]
        public string kavNoField { get; set; }

        [StringLength(50)]
        public string kadasterType { get; set; }

        [StringLength(50)]
        public string diagramaticType { get; set; }

        public int entityID { get; set; }
    }
}
