﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_City")]
    public partial class MS_City
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string cityName { get; set; }

        [ForeignKey("MS_County")]
        public int countyID { get; set; }
        public virtual MS_County MS_County { get; set; }
    }
}
