﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Cluster")]
    public partial class MS_Cluster
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string clusterCode { get; set; }

        [Required]
        [StringLength(35)]
        public string clusterName { get; set; }

        public DateTime? dueDateDevelopment { get; set; }

        [StringLength(500)]
        public string dueDateRemarks { get; set; }

        public int sortNo { get; set; }

        [StringLength(100)]
        public string handOverPeriod { get; set; }

        [StringLength(100)]
        public string gracePeriod { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public double penaltyRate { get; set; }

        public int startPenaltyDay { get; set; }

        public string termConditionFile { get; set; }

        public string website { get; set; }
    }
}
