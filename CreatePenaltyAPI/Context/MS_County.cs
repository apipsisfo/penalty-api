﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_County")]
    public partial class MS_County
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string countyName { get; set; }

        [ForeignKey("MS_Territory")]
        public int territoryID { get; set; }
        public virtual MS_Territory MS_Territory { get; set; }
    }
}
