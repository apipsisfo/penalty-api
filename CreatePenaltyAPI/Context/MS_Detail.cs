﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Detail")]
    public partial class MS_Detail
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string detailCode { get; set; }

        [Required]
        [StringLength(50)]
        public string detailName { get; set; }

        [StringLength(200)]
        public string detailImage { get; set; }

        public int? isMultiple { get; set; }
        
    }
}
