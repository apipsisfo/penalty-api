﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    public class MS_Facade
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string facadeCode { get; set; }

        [Required]
        [StringLength(50)]
        public string facadeName { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public int clusterID { get; set; }

        public int detailID { get; set; }      
    }
}
