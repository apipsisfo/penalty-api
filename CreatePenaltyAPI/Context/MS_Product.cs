﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Product")]
    public partial class MS_Product
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(5)]
        public string productCode { get; set; }

        [Required]
        [StringLength(30)]
        public string productName { get; set; }

        [Required]
        public int sortNo { get; set; }
    }
}
