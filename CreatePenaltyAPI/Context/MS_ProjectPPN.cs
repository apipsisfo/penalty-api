﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_ProjectPPN")]
    public partial class MS_ProjectPPN
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }

        public double ppnValue { get; set; }

        public DateTime? startDate { get; set; }

        public DateTime? endDate { get; set; }

        public virtual MS_Project MS_Project { get; set; }
    }
}
