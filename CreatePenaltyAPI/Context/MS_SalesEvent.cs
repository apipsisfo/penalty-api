﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_SalesEvent")]
    public partial class MS_SalesEvent
    {
        [Key]
        public int Id { get; set; }
        public int EntityID { get; set; }

        [Required]
        [StringLength(5)]
        public string eventCode { get; set; }

        [Required]
        [StringLength(50)]
        public string eventName { get; set; }
        
        public int sortNo { get; set; }
    }
}
