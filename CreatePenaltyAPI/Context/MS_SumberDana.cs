﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    public class MS_SumberDana
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [Required]
        [StringLength(3)]
        public string sumberDanaCode { get; set; }

        [Required]
        [StringLength(50)]
        public string sumberDanaName { get; set; }

        [Required]
        public int sort { get; set; }
    }
}
