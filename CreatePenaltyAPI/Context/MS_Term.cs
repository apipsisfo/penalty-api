﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CreatePenaltyAPI.Context
{
    [Table("MS_Term")]
    public partial class MS_Term
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [StringLength(5)]
        public string termCode { get; set; }

        public short termNo { get; set; }

        public short PPJBDue { get; set; }

        [Required]
        [StringLength(200)]
        public string remarks { get; set; }

        public int? termInstallment { get; set; }

        public bool isActive { get; set; }

        public int sortNo { get; set; }

        [Required]
        [StringLength(1)]
        public string discBFCalcType { get; set; }

        [Required]
        [StringLength(1)]
        public string DPCalcType { get; set; }

        [StringLength(5)]
        public string GroupTermCode { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        [ForeignKey("MS_TermMain")]
        public int termMainID { get; set; }
        public virtual MS_TermMain MS_TermMain { get; set; }

    }
}
