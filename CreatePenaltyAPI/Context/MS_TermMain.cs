﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_TermMain")]
    public partial class MS_TermMain
    {
        [Key]
        public int Id { get; set; }
        [StringLength(5)]
        public string termCode { get; set; }

        public int entityID { get; set; }

        [Required]
        [StringLength(200)]
        public string termDesc { get; set; }

        [Required]
        [StringLength(5)]
        public string famDiscCode { get; set; }

        [Required]
        [Column(TypeName = "money")]
        public decimal BFAmount { get; set; }

        [Required]
        [StringLength(200)]
        public string remarks { get; set; }

    }
}
