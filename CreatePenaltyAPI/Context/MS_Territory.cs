﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Territory")]
    public partial class MS_Territory
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string territoryName { get; set; }
    }
}
