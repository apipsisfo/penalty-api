﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_TransFrom")]
    public partial class MS_TransFrom
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [Required]
        [StringLength(5)]
        public string transCode { get; set; }

        [Required]
        [StringLength(5)]
        public string parentTransCode { get; set; }

        [Required]
        [StringLength(40)]
        public string transName { get; set; }

        [Required]
        [StringLength(100)]
        public string transDesc { get; set; }
    }
}
