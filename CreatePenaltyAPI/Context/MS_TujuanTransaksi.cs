﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    public class MS_TujuanTransaksi
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [Required]
        [StringLength(3)]
        public string tujuanTransaksiCode { get; set; }

        [Required]
        [StringLength(50)]
        public string tujuanTransaksiName { get; set; }

        public int sort { get; set; }  

    }
}
