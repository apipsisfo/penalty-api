﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_UnitCode")]
    public partial class MS_UnitCode
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [StringLength(20)]
        public string unitCode { get; set; }

        [Required]
        [StringLength(50)]
        public string unitName { get; set; }

        [ForeignKey("MS_Project")]
        public int projectID { get; set; }
        public virtual MS_Project MS_Project { get; set; }

        public string floor { get; set; }
    }
}
