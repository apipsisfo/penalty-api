﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CreatePenaltyAPI.Context
{
    [Table("MS_Zoning")]
    public partial class MS_Zoning
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        //unique
        [Required]
        [StringLength(8)]
        public string zoningCode { get; set; }

        [Required]
        [StringLength(50)]
        public string zoningName { get; set; }

        public ICollection<MS_Unit> MS_Unit { get; set; }

    }
}
