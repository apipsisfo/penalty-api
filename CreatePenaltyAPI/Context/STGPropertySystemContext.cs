﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;



namespace CreatePenaltyAPI.Context
{
    public partial class STGPropertySystemContext : DbContext
    {

        public STGPropertySystemContext()
        {
        }

        public STGPropertySystemContext(DbContextOptions<STGPropertySystemContext> options)
            : base(options)
        {
        }

        public virtual DbSet<LK_Alloc> LK_Alloc { get; set; }
        public virtual DbSet<TR_BookingHeader> TR_BookingHeader { get; set; }
        public virtual DbSet<TR_BookingDetail> TR_BookingDetail { get; set; }
        public virtual DbSet<TR_BookingDetailSchedule> TR_BookingDetailSchedule { get; set; }
        public virtual DbSet<TR_PenaltySchedule> TR_PenaltySchedule { get; set; }
        public virtual DbSet<Temp_Procces_Penalty> Temp_Procces_Penalty { get; set; }
        public virtual DbSet<LOG_ProsesCreatePenaltyHeader> LOG_ProsesCreatePenaltyHeader { get; set; }
        public virtual DbSet<LOG_ProsesCreatePenaltyDetail> LOG_ProsesCreatePenaltyDetail { get; set; }
        public virtual DbSet<MS_Unit> MS_Unit { get; set; }
        public virtual DbSet<MS_ProjectPPN> MS_ProjectPPN { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server=18.141.1.119,2121;Database=STGPropertySystem;User Id= SA; Password=Mkt@!@#$%;");
//            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");          

            //OnModelCreatingPartial(modelBuilder);
        }
        
    }
}
