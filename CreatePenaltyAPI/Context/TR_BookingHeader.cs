﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("TR_BookingHeader")]
    public partial class TR_BookingHeader
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [Required]
        [StringLength(20)]
        public string bookCode { get; set; }

        [ForeignKey("MS_Unit")]
        public int unitID { get; set; }
        public virtual MS_Unit MS_Unit { get; set; }

        public DateTime bookDate { get; set; }

        public DateTime? cancelDate { get; set; }

        [Required]
        [StringLength(8)]
        public string psCode { get; set; }

        [Required]
        [StringLength(3)]
        public string scmCode { get; set; }

        [Required]
        [StringLength(12)]
        public string memberCode { get; set; }

        [Required]
        [StringLength(100)]
        public string memberName { get; set; }

        [Required(AllowEmptyStrings = true)]
        public string NUP { get; set; }

        public bool? isSK { get; set; }

        [ForeignKey("MS_SalesEvent")]
        public int eventID { get; set; }
        public virtual MS_SalesEvent MS_SalesEvent { get; set; }

        [ForeignKey("MS_Transfrom")]
        public int transID { get; set; }
        public virtual MS_TransFrom MS_TransFrom { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(3)]
        public string BFPayTypeCode { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(30)]
        public string bankNo { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(50)]
        public string bankName { get; set; }

        //public int termID { get; set; }
        [ForeignKey("MS_Term")]
        public int termID { get; set; }
        public virtual MS_Term MS_Term { get; set; }

        //public short termNo { get; set; }

        public short PPJBDue { get; set; }

        [Required]
        [StringLength(200)]
        public string termRemarks { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(1500)]
        public string remarks { get; set; }

        [Column(TypeName = "money")]
        public decimal netPriceComm { get; set; }

        [Required(AllowEmptyStrings = true)]
        [StringLength(5)]
        public string KPRBankCode { get; set; }

        public bool isPenaltyStop { get; set; }

        public bool isSMS { get; set; }

        //public int shopBusinessID { get; set; }

        [ForeignKey("MS_ShopBusiness")]
        public int shopBusinessID { get; set; }
        public virtual MS_ShopBusiness MS_ShopBusiness { get; set; }

        [ForeignKey("LK_SADStatus")]
        public int SADStatusID { get; set; }
        public virtual LK_SADStatus LK_SADStatus { get; set; }

        [ForeignKey("LK_Promotion")]
        public int promotionID { get; set; }
        public virtual LK_Promotion LK_Promotion { get; set; }

        [Required]
        [StringLength(1)]
        public string discBFCalcType { get; set; }

        [Required]
        [StringLength(1)]
        public string DPCalcType { get; set; }

        [ForeignKey("MS_SumberDana")]
        public int? sumberDanaID { get; set; }
        public virtual MS_SumberDana MS_SumberDana { get; set; }

        [ForeignKey("MS_TujuanTransaksi")]
        public int? tujuanTransaksiID { get; set; }
        public virtual MS_TujuanTransaksi MS_TujuanTransaksi { get; set; }

        [StringLength(50)]
        public string nomorRekeningPemilik { get; set; }

        [StringLength(50)]
        public string bankRekeningPemilik { get; set; }

        [ForeignKey("MS_Facade")]
        public int? facadeID { get; set; }
        public virtual MS_Facade MS_Facade { get; set; }
    }
}
