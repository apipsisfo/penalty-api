﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("TR_PenaltySchedule")]
    public partial class TR_PenaltySchedule
    {
        [Key]
        public int Id { get; set; }
        public int entityID { get; set; }

        [ForeignKey("TR_BookingHeader")]
        public int bookingHeaderID { get; set; }
        public virtual TR_BookingHeader TR_BookingHeader { get; set; }

        public int SeqNo { get; set; }

        public int ScheduleTerm { get; set; }

        [Required]
        [StringLength(50)]
        public string ScheduleAllocCode { get; set; }

        [Column(TypeName = "money")]
        public decimal ScheduleNetAmount { get; set; }

        public int penaltyAging { get; set; }

        [Column(TypeName = "money")]
        public decimal penaltyAmount { get; set; }

        [Required]
        [StringLength(11)]
        public string scheduleStatus { get; set; }

        [Required]
        [StringLength(11)]
        public string penaltyStatus { get; set; }
    }
}
