﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Context
{
    [Table("Temp_Procces_Penalty")]
    public class Temp_Procces_Penalty
    {
        [Key]
        public string Bookcode { get; set; }
        public int ProjectID { get; set; }
        public DateTime InputTime { get; set; }
    }
}
