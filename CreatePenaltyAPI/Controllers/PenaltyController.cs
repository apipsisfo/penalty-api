﻿using CreatePenaltyAPI.Model;
using CreatePenaltyAPI.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/penalty")]
    [ApiController]
    public class PenaltyController : ControllerBase
    {
        private readonly IPenaltyService _penaltyService;

        public PenaltyController(IPenaltyService penaltyService)
        {
            _penaltyService = penaltyService;
        }
  
        [AllowAnonymous]
        [HttpPost("procces-penalty")]
        public async Task<ActionResult<CreatePenaltyResultDto>> Create([FromBody] ParamsDto content)
        {
            try
            {
                CreatePenaltyResultDto model = await _penaltyService.CreatePenalty(content);
                return Ok(model);
            }
            catch (Exception e)
            {

                throw;
            }

        }

        [AllowAnonymous]
        [HttpPost("procces-penalty-all")]
        public async Task<ActionResult<CreatePenaltyResultDto>> CreateAll()
        {
            try
            {
                CreatePenaltyResultDto model = await _penaltyService.CreatePenaltyAllProject();
                return Ok(model);
            }
            catch (Exception e)
            {

                throw;
            }

        }

        [AllowAnonymous]
        [HttpPost("procces-penalty-manual")]
        public async Task<ActionResult<CreatePenaltyResultDto>> CreateManual([FromBody] List<string> listBookcode)
        {
            try
            {
                CreatePenaltyResultDto model = await _penaltyService.CreatePenaltyManual(listBookcode);
                return Ok(model);
            }
            catch (Exception e)
            {

                throw;
            }

        }
    }
}
