﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreatePenaltyAPI.Model;
using CreatePenaltyAPI.Services.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreatePenaltyAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/recalculatebalance")]
    [ApiController]
    public class RecalculateBalanceController : ControllerBase
    {
        private readonly IRecalculateBalanceService _recalculateBalanceService;

        public RecalculateBalanceController(IRecalculateBalanceService recalculateBalanceService)
        {
            _recalculateBalanceService = recalculateBalanceService;
        }

        [AllowAnonymous]
        [HttpPost("recalculate-balance-all")]
        public async Task<ActionResult<RecalculateBalanceResultDto>> CreateRecalculateBalanceAll()
        {
            try
            {
                RecalculateBalanceResultDto model = await _recalculateBalanceService.RecalculateBalanceAllProject();
                return Ok(model);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [AllowAnonymous]
        [HttpPost("recalculate-balance-manual")]
        public async Task<ActionResult<RecalculateBalanceResultDto>> CreateRecalculateBalanceManual([FromBody] List<string> listBookcode)
        {
            try
            {
                RecalculateBalanceResultDto model = await _recalculateBalanceService.RecalculateBalanceManual(listBookcode);
                return Ok(model);
            }
            catch (Exception e)
            {
                throw e;
            }
        }        
    }
}
