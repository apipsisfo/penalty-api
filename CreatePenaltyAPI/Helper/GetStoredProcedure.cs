﻿using CreatePenaltyAPI.Helper.Interface;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Helper
{
    public class GetStoredProcedure : IGetStoredProcedure
    {
        private readonly IConfiguration _appConfiguration;

        public GetStoredProcedure(IConfiguration configuration)
        {
            _appConfiguration = configuration;
        }

        public IReadOnlyList<T> ExecSP<T>(string procedure, object paramater)
        {
            try
            {
                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");

                using (IDbConnection db = new SqlConnection(connection))
                {
                    return db.Query<T>(procedure, paramater, commandType: CommandType.StoredProcedure).ToList();
                }

            }
            catch (Exception e)
            {

                throw;
            }

        }

        public IReadOnlyList<T> SelectQuery<T>(string procedure, object paramater)
        {
            try
            {
                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");

                using (IDbConnection db = new SqlConnection(connection))
                {
                    return db.Query<T>(procedure, paramater).ToList();
                }

            }
            catch (Exception e)
            {

                throw;
            }

        }
    }
}
