﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Helper.Interface
{
    public interface IGetStoredProcedure
    {
        IReadOnlyList<T> ExecSP<T>(string procedure, object paramater);
        IReadOnlyList<T> SelectQuery<T>(string procedure, object paramater);
    }
}
