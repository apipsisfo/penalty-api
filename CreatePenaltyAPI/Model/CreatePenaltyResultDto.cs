﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Model
{
    public class CreatePenaltyResultDto
    {
        public int Succes { get; set; }
        public int Fail { get; set; }
        public List<ListBookcodeFailDto> ListBookcodeFail { get; set; }
    }
    public class ListBookcodeFailDto
    {
        public string Bookcode { get; set; }
        public string FailMessage { get; set; }
    }

    public class CreatePenaltyPerBookcodeResultDto
    {
        public string FailMessage { get; set; }
    }
}
