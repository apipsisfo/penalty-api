﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Model
{
    public class GetPSASParamsDto
    {
        public string bookCode { get; set; }
        public string unitCode { get; set; }
        public string unitNo { get; set; }
    }
    public class ListBookingScheduleDto
    {
        public List<GetPSASParamsBookingScheduleDto> ListBookSchedule { get; set; }
    }
    public class GetPSASParamsBookingScheduleDto
    {
        public int Id { get; set; }
        public string bookCode { get; set; }
        public int bookingHeaderID { get; set; }
        public int bookingDetailID { get; set; }
        public int schedNo { get; set; }
        public DateTime dueDate { get; set; }
        public decimal netAmt { get; set; }
        public decimal netOut { get; set; }
        public decimal vatAmt { get; set; }
        public decimal vatOut { get; set; }
        public double? pctTax { get; set; }
    }
    public class ListPaymentDto
    {
        public List<GetPSASParamPaymentDto> ListPayment { get; set; }
    }
    public class GetPSASParamPaymentDto
    {
        public int PaymentHeaderID { get; set; }
        public int PaymentDetailID { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime Cleardate { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal AmountNet { get; set; }
        public decimal AmountVat { get; set; }
        public decimal Amount { get; set; }
        public bool isAdj { get; set; }
    }
    public class ListpenaltyScheduleDto
    {
        public List<GetPSASParamPenaltySchedule> ListpenaltySchedule { get; set; }
    }
    public class GetPSASParamPenaltySchedule
    {
        public int bookingHeaderID { get; set; }
        public int ScheduleTerm { get; set; }
        public int Aging { get; set; }
        public decimal PenaltyAmount { get; set; }
        public string ScheduleStatus { get; set; }
        public string PenaltyStatus { get; set; }
    }
    public class ListPaymentDetailAllocTemp
    {
        public List<PaymentDetaiAllocTemp> ListPaymentDetailAlloc { get; set; }
    }
    public class PaymentDetaiAllocTemp
    {
        public DateTime CreationTime { get; set; }
        public int CreatorUserId { get; set; }
        public DateTime LastModificationTime { get; set; }
        public int LastModifierUserId { get; set; }
        public int entityID { get; set; }
        public decimal netAmt { get; set; }
        public decimal vatAmt { get; set; }
        public int paymentDetailID { get; set; }
        public int schedNo { get; set; }
    }
}
