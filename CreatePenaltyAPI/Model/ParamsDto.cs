﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Model
{
    public class ParamsDto
    {
        public int projectId { get; set; }
        public int userId { get; set; }
    }

    public class ParamsAllDto
    {        
        public int userId { get; set; }
    }
}
