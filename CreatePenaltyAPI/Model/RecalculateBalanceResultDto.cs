﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Model
{
    public class RecalculateBalanceResultDto
    {
        public int Succes { get; set; }
        public int Fail { get; set; }
        public List<RecalculateBalanceListBookcodeFailDto> ListBookcodeFail { get; set; }
    }
    public class RecalculateBalanceListBookcodeFailDto
    {
        public string Bookcode { get; set; }
        public string FailMessage { get; set; }
    }

    public class RecalculateBalancePerBookcodeResultDto
    {
        public string FailMessage { get; set; }
    }
}
