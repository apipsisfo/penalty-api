﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Model
{
    public class ResultDto
    {
        public bool Flag { get; set; }
        public string Message { get; set; }
    }
}
