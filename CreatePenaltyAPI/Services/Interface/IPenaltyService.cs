﻿using CreatePenaltyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Services.Interface
{
    public interface IPenaltyService
    {
        //Task<string> GetData();
        Task<CreatePenaltyResultDto> CreatePenalty(ParamsDto content);
        Task<CreatePenaltyResultDto> CreatePenaltyAllProject();
        Task<CreatePenaltyResultDto> CreatePenaltyManual(List<string> listBookCode);
    }
}
