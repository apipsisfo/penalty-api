﻿using CreatePenaltyAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Services.Interface
{
    public interface IRecalculateBalanceService
    {
        Task<RecalculateBalanceResultDto> RecalculateBalanceAllProject();
        Task<RecalculateBalanceResultDto> RecalculateBalanceManual(List<string> listBookCode);
    }
}
