﻿using CreatePenaltyAPI.Context;
using CreatePenaltyAPI.Helper.Interface;
using CreatePenaltyAPI.Model;
using CreatePenaltyAPI.Services.Interface;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Services 
{
    public class PenaltyService : IPenaltyService
    {
        private readonly STGPropertySystemContext _context;
        private readonly IConfiguration _appConfiguration;
        private readonly IGetStoredProcedure _getStoredProcedure;

        public PenaltyService(
                STGPropertySystemContext sTGPropertySystemContext, 
                IConfiguration configuration,
                IGetStoredProcedure getStoredProcedure)
        {
            _context = sTGPropertySystemContext;
            _appConfiguration = configuration;
            _getStoredProcedure = getStoredProcedure;
        }

        private string GetEnvironment()
        {
            var appsettingsjson = JObject.Parse(File.ReadAllText("appsettings.json"));
            var webConfigApp = (JObject)appsettingsjson["App"];

            var env = webConfigApp.Property("env").Value.ToString();

            return env;
        }

        private DataTable ColumnsLOG(DataTable dt)
        {
            dt.Columns.Add("createdDate", typeof(DateTime));
            dt.Columns.Add("bookCode", typeof(string));
            dt.Columns.Add("message_ex", typeof(string));

            return dt;
        }

        public async Task<CreatePenaltyResultDto> CreatePenaltyManual(List<string> listBookCode)
        {
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);
            CreatePenaltyResultDto result = new CreatePenaltyResultDto();
            List<ListBookcodeFailDto> listRow = new List<ListBookcodeFailDto>();

            int succes = 0;
            int fail = 0;

            try
            { 
                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        string bookCode = data;
                        ListBookcodeFailDto temp = new ListBookcodeFailDto();
                        try
                        {
                            DataPenaltyListDto Model = new DataPenaltyListDto();

                            var procedure = "[SP_GetDataPenalty]";
                            var values = new { bookCode = bookCode };

                            var penaltyList = _getStoredProcedure.ExecSP<DataPenaltyDto>(procedure, values);
                            Model.PenaltyList = penaltyList.ToList();

                            CreatePenaltyPerBookcodeResultDto resultPerBckCode = await DoThisCreatePenalty(Model, 2);
                            if (resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data, resultPerBckCode.FailMessage);
                            }

                            await ProsesAdj(Model, 2);
                            succes += 1;
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }

                        //var deleteData = await _context.Temp_Procces_Penalty.FindAsync(bookCode);
                        //_context.Temp_Procces_Penalty.Remove(deleteData);
                        //await _context.SaveChangesAsync();
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;

                await SaveLogRow(result);
            }
            catch (Exception e)
            {

                //throw;
            }


            return result;
        }

        public async Task<CreatePenaltyResultDto> CreatePenaltyAllProject()
        {
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);
            CreatePenaltyResultDto result = new CreatePenaltyResultDto();
            List<ListBookcodeFailDto> listRow = new List<ListBookcodeFailDto>();

            var env = GetEnvironment();

            int succes = 0;
            int fail = 0;
            try
            {
                var getBookcode = "[SP_GetDataBookCodePerSetup]";
                var flag = new { flag = "PEN" };
          
                var bookcodes = _getStoredProcedure.ExecSP<string>(getBookcode, flag);
                var listBookCode = bookcodes.ToList();

                if(env == "dev")
                {
                    listBookCode = (from a in _context.Temp_Procces_Penalty
                                    select a.Bookcode).ToList();
                }

                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        string bookCode = data;
                        ListBookcodeFailDto temp = new ListBookcodeFailDto();
                        try
                        {
                            DataPenaltyListDto Model = new DataPenaltyListDto();

                            var procedure = "[SP_GetDataPenalty]";
                            var values = new { bookCode = bookCode };

                            var penaltyList = _getStoredProcedure.ExecSP<DataPenaltyDto>(procedure, values);
                            Model.PenaltyList = penaltyList.ToList();

                            CreatePenaltyPerBookcodeResultDto resultPerBckCode = await DoThisCreatePenalty(Model, 2);
                            if (resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data, resultPerBckCode.FailMessage);
                            }

                            await ProsesAdj(Model, 2);
                            succes += 1;
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }

                        //var deleteData = await _context.Temp_Procces_Penalty.FindAsync(bookCode);
                        //_context.Temp_Procces_Penalty.Remove(deleteData);
                        //await _context.SaveChangesAsync();
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;

                await SaveLogRow(result);
            }
            catch (Exception e)
            {

                //throw;
            }


            return result;
        }

        private DataTable ColumnsLogRow(DataTable dt)
        {
            dt.Columns.Add("HeaderId", typeof(int));
            dt.Columns.Add("CreatedDate", typeof(DateTime));
            dt.Columns.Add("Bookcode", typeof(string));
            dt.Columns.Add("FailMessage", typeof(string));

            return dt;
        }

        public async Task SaveLogRow(CreatePenaltyResultDto input)
        {
            DataTable dtDetailLog = new DataTable();
            ColumnsLogRow(dtDetailLog);

            try
            {
                //if(input.Succes != 0 && input.Fail != 0)
                //{
                    #region header
                    LOG_ProsesCreatePenaltyHeader header = new LOG_ProsesCreatePenaltyHeader();
                    header.CreatedDate = DateTime.Now;
                    header.Fail = input.Fail;
                    header.Succes = input.Succes;

                    _context.Add(header);
                    await _context.SaveChangesAsync();

                        #region detail
                        if (input.ListBookcodeFail.Count > 0)
                        {
                            foreach (var detail in input.ListBookcodeFail)
                            {
                                dtDetailLog.Rows.Add(header.Id, DateTime.Now, detail.Bookcode, detail.FailMessage);
                            }

                            BulkInsertLogRow(dtDetailLog);
                        }
                        #endregion

                    #endregion
                //}

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void BulkInsertLogRow(DataTable dt)
        {                          
            string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
            //string connection = "Persist Security Info=True;Data Source=10.12.1.43; Initial Catalog=STGPropertySystem; User ID=sa;Password=Password1!";
            SqlConnection con = new SqlConnection(connection);
            //create object of SqlBulkCopy which help to insert  
            SqlBulkCopy objbulk = new SqlBulkCopy(con);

            try
            {
                //assign Destination table name  
                objbulk.DestinationTableName = "LOG_ProsesCreatePenaltyDetail";

                objbulk.ColumnMappings.Add("HeaderId", "HeaderId");
                objbulk.ColumnMappings.Add("CreatedDate", "CreatedDate");
                objbulk.ColumnMappings.Add("Bookcode", "Bookcode");
                objbulk.ColumnMappings.Add("FailMessage", "FailMessage");       

                con.Open();
                //insert bulk Records into DataBase.  
                objbulk.WriteToServer(dt);

                //Logger.DebugFormat("CreatePenaltyNew() count: " + dt.Rows.Count);
            }
            catch (Exception ex)
            {
                    
                //Logger.DebugFormat("CreatePenaltyNew() " + ex);

            }
            finally
            {
                    
                con.Close();
            }
                            
        }

        public async Task<CreatePenaltyResultDto> CreatePenalty(ParamsDto content)
        {
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);
            CreatePenaltyResultDto result = new CreatePenaltyResultDto();
            List<ListBookcodeFailDto> listRow = new List<ListBookcodeFailDto>();

            int succes = 0;
            int fail = 0;
            try
            {
                var listBookCode = _context.Temp_Procces_Penalty.Where(ss => ss.ProjectID == content.projectId).ToList();

                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        string bookCode = data.Bookcode;
                        ListBookcodeFailDto temp = new ListBookcodeFailDto();
                        try
                        {
                            DataPenaltyListDto Model = new DataPenaltyListDto();
                                                   
                            var procedure = "[SP_GetDataPenalty]";
                            var values = new { bookCode = bookCode };

                            var penaltyList = _getStoredProcedure.ExecSP<DataPenaltyDto>(procedure, values);
                            Model.PenaltyList = penaltyList.ToList();

                            CreatePenaltyPerBookcodeResultDto resultPerBckCode = await DoThisCreatePenalty(Model, content.userId);
                            if (resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data.Bookcode, resultPerBckCode.FailMessage);
                            }

                            await ProsesAdj(Model, content.userId);
                            succes += 1;
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data.Bookcode;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data.Bookcode, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }
                        
                        var deleteData = await _context.Temp_Procces_Penalty.FindAsync(bookCode);
                        _context.Temp_Procces_Penalty.Remove(deleteData);
                        await _context.SaveChangesAsync();
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;
            }
            catch (Exception e)
            {

                //throw;
            }


            return result;
        }

        private DataTable ColumnsPenaltySchedule(DataTable dt)
        {
            dt.Columns.Add("CreationTime", typeof(DateTime));
            dt.Columns.Add("CreatorUserId", typeof(long));
            dt.Columns.Add("ScheduleAllocCode", typeof(string));
            dt.Columns.Add("ScheduleNetAmount", typeof(decimal));
            dt.Columns.Add("ScheduleTerm", typeof(int));
            dt.Columns.Add("SeqNo", typeof(int));
            dt.Columns.Add("bookingHeaderID", typeof(int));
            dt.Columns.Add("entityID", typeof(int));
            dt.Columns.Add("penaltyAging", typeof(int));
            dt.Columns.Add("penaltyAmount", typeof(decimal));
            dt.Columns.Add("scheduleStatus", typeof(string));
            dt.Columns.Add("penaltyStatus", typeof(string));


            return dt;
        }

        private async Task<CreatePenaltyPerBookcodeResultDto> DoThisCreatePenalty(DataPenaltyListDto Model, int userID)
        {
            CreatePenaltyPerBookcodeResultDto result = new CreatePenaltyPerBookcodeResultDto();
            result.FailMessage = "";
            DataTable dt = new DataTable();
            ColumnsPenaltySchedule(dt);

            decimal amountAfter = 0m;
            double agingAfter = 0;
            decimal totalPenaltyAfter = 0;
            double power = 0;
            decimal Net = decimal.Parse(1.1.ToString());
            decimal Vat = decimal.Parse(0.1.ToString());
            //int scheduleCekSameDay = 0;            

            var grouping = (from a in Model.PenaltyList.Where(ss => ss.totalAmount >= 0)
                            group a by new { a.bookingHeaderID, a.bookingDetailID, a.schedNo, a.dueDate } into g
                            select new SummaryPenaltyDto
                            {
                                schedNo = g.Key.schedNo,
                                dueDate = g.Key.dueDate,
                                bookingHeaderID = g.Key.bookingHeaderID,
                                bookingDetailID = g.Key.bookingDetailID,
                                totalAmountSchedule = g.Max(xx => xx.totalAmountSchedule),
                                totalAmount = g.Sum(xx => xx.totalAmount),
                                totalPenalty = g.Sum(xx => xx.totalPenalty),
                                aging = g.Max(xx => xx.aging),
                                penaltyRate = g.Max(xx => xx.penaltyRate),
                                penaltyFreq = g.Max(xx => xx.penaltyFreq),
                                penaltyBaseRate = g.Max(xx => xx.penaltyBaseRate),
                                allocCode = g.Max(xx => xx.allocCode),
                                entityID = g.Max(xx => xx.entityID),
                                isAdj = g.Max(xx => xx.isAdj)
                            }).ToList();

            for (int i = 0; i < grouping.Count; i++)
            {
                amountAfter = Math.Round(grouping[i].totalAmountSchedule - grouping[i].totalAmount, 4);

                try
                {
                    if (amountAfter > 0)
                    {

                        agingAfter = (DateTime.Now.Date - grouping[i].dueDate.Date).TotalDays;
                        power = Math.Pow(Convert.ToDouble(1 + (grouping[0].penaltyRate / grouping[0].penaltyBaseRate)), agingAfter);
                        decimal agingConvert = Math.Round(Convert.ToDecimal(agingAfter), 0);
                        decimal powerConvert = Convert.ToDecimal(power);

                        //hitung lagi penalty jika masih ada outstanding
                        totalPenaltyAfter = grouping[0].penaltyFreq.ToUpper() == "MONTHLY" ?
                                            (amountAfter * agingConvert) * ((grouping[0].penaltyRate / grouping[0].penaltyBaseRate) / 30) :
                                            (amountAfter) * (powerConvert - 1);

                        if (agingAfter > 0)
                        {
                            dt.Rows.Add(
                                                     DateTime.Now,                        //creationTime
                                                     userID,                              //creatorUserID
                                                     grouping[i].allocCode,             //scheduleAllocCode
                                                     grouping[i].totalAmountSchedule,           //scheduleNetAmount
                                                     grouping[i].schedNo,               //scheduleTerm
                                                     grouping[i].schedNo,               //seqNo
                                                     grouping[i].bookingHeaderID,       //bookingHeaderID
                                                     1,              //entityID
                                                     Convert.ToInt32(agingConvert),                       //penaltyAging
                                                     grouping[i].totalPenalty + totalPenaltyAfter,    //penaltyAmount
                                                     "Outstanding",                              //scheduleStatus                    
                                                     "Outstanding"                               //penaltyStatus
                                                 );
                        }

                    }
                    else
                    {
                        if (grouping[i].totalPenalty >= 0 && grouping[i].aging > 0 || grouping[i].isAdj == true && grouping[i].aging > 0)
                        {
                            dt.Rows.Add(
                                            DateTime.Now,                        //creationTime
                                            userID,                               //creatorUserID
                                            grouping[i].allocCode,             //scheduleAllocCode
                                            grouping[i].totalAmountSchedule,           //scheduleNetAmount
                                            grouping[i].schedNo,               //scheduleTerm
                                            grouping[i].schedNo,               //seqNo
                                            grouping[i].bookingHeaderID,       //bookingHeaderID
                                            1,              //entityID
                                            grouping[i].aging,         //penaltyAging
                                            grouping[i].totalPenalty,           //penaltyAmount
                                            "Fully Paid",                              //scheduleStatus                    
                                            "Outstanding"                               //penaltyStatus
                                        );
                        }

                    }
                }
                catch (Exception e)
                {
                    break;
                    throw;
                }

            }

            var flagBulkUpadate = await BulkUpadate(grouping.FirstOrDefault().bookingHeaderID);
            var flagBulkInsert = await BulkInsert(dt);
            var flagRefreshPenalty = await RefreshPenalty(grouping.FirstOrDefault().bookingHeaderID);
            var flagWaivePenalty = await WaivePenalty(grouping.FirstOrDefault().bookingHeaderID);

            int bookingHeaderID = grouping[0].bookingHeaderID;
            int bookingDetailID = grouping[0].bookingDetailID;
            var flagUpdateBookingSchedule = await UpdateBookingSchedule(bookingHeaderID, bookingDetailID, userID);

            result.FailMessage = FailMessage(flagBulkUpadate, flagBulkInsert, flagRefreshPenalty, flagWaivePenalty, flagUpdateBookingSchedule);
            return result;
        }

        private async Task ProsesAdj(DataPenaltyListDto Model, int userID)
        {
            var adj = Model.PenaltyList.Where(ss => ss.isAdj == false && ss.totalAmount < 0).OrderBy(ss => ss.clearDate).ToList();

            for (int i = 0; i < adj.Count; i++)
            {
                var endPay = Model.PenaltyList.Where(ss => ss.totalAmount > 0 && ss.isAdj == false && ss.clearDate < adj[i].clearDate && ss.schedNo == adj[i].schedNo).OrderByDescending(ss => ss.clearDate).ThenByDescending(ss => ss.creationTime).FirstOrDefault();

                endPay.totalAmount = 0;
                endPay.totalPenalty = 0;
                endPay.isAdj = true;
                //endPay.isPenaltyAfterAdj = true;
            }

            await DoThisCreatePenalty(Model, userID);
        }

        private async Task<bool> BulkUpadate(int ID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
                SqlConnection con = new SqlConnection(connection);

                using (SqlCommand command = new SqlCommand("", con))
                {
                    try
                    {
                        con.Open();
                        //Creating temp table on database
                        command.CommandText = "DELETE TR_PenaltySchedule WHERE bookingHeaderID =" + ID + " and penaltyStatus = 'Outstanding' or bookingHeaderID =" + ID + " and scheduleStatus = 'Outstanding'";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        // Handle exception properly
                    }
                    finally
                    {
                        flag = true;
                        con.Close();
                    }
                }

                return flag;
            });
        }

        private async Task<bool> BulkInsert(DataTable dt)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
                //string connection = "Persist Security Info=True;Data Source=10.12.1.43; Initial Catalog=STGPropertySystem; User ID=sa;Password=Password1!";
                SqlConnection con = new SqlConnection(connection);
                //create object of SqlBulkCopy which help to insert  
                SqlBulkCopy objbulk = new SqlBulkCopy(con);

                try
                {
                    //assign Destination table name  
                    objbulk.DestinationTableName = "TR_PenaltySchedule";

                    objbulk.ColumnMappings.Add("CreationTime", "CreationTime");
                    objbulk.ColumnMappings.Add("CreatorUserId", "CreatorUserId");
                    objbulk.ColumnMappings.Add("ScheduleAllocCode", "ScheduleAllocCode");
                    objbulk.ColumnMappings.Add("ScheduleNetAmount", "ScheduleNetAmount");
                    objbulk.ColumnMappings.Add("ScheduleTerm", "ScheduleTerm");
                    objbulk.ColumnMappings.Add("SeqNo", "SeqNo");
                    objbulk.ColumnMappings.Add("bookingHeaderID", "bookingHeaderID");
                    objbulk.ColumnMappings.Add("entityID", "entityID");
                    objbulk.ColumnMappings.Add("penaltyAging", "penaltyAging");
                    objbulk.ColumnMappings.Add("penaltyAmount", "penaltyAmount");
                    objbulk.ColumnMappings.Add("scheduleStatus", "scheduleStatus");
                    objbulk.ColumnMappings.Add("penaltyStatus", "penaltyStatus");

                    con.Open();
                    //insert bulk Records into DataBase.  
                    objbulk.WriteToServer(dt);

                    //Logger.DebugFormat("CreatePenaltyNew() count: " + dt.Rows.Count);
                }
                catch (Exception ex)
                {
                    flag = false;
                    //Logger.DebugFormat("CreatePenaltyNew() " + ex);

                }
                finally
                {
                    flag = true;
                    con.Close();
                }

                return flag;
            });
        }

        private async Task<bool> RefreshPenalty(int bookingHeaderID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                var procedure = "[SP_RefreshDataPenalty]";
                var values = new { bookingHeaderID = bookingHeaderID };

                try
                {

                    var status = _getStoredProcedure.ExecSP<int>(procedure, values);

                    //Logger.DebugFormat("RefreshPenalty() - Ended RefreshPenalty");
                    flag = true;

                }
                catch (Exception ex)
                {
                    flag = false;
                    throw;
                }

                return flag;
            });


        }

        private async Task<bool> UpdateBookingSchedule(int bookingHeaderID, int bookingDetailID, int userID)
        {
            bool flag = false;
            var NetPrice = (from a in _context.TR_BookingDetail.Where(ss => ss.bookingHeaderID == bookingHeaderID)
                            join b in _context.TR_BookingHeader on a.bookingHeaderID equals b.Id
                            select new
                            {
                                idBookingDetail = a.Id,
                                netNetPrice = a.netNetPrice
                            }
                            ).ToList();



            var sumNetPrice = NetPrice.Sum(ss => ss.netNetPrice);
            var sumPenaltyAmount = _context.TR_PenaltySchedule.Where(ss => ss.bookingHeaderID == bookingHeaderID && ss.penaltyStatus.ToUpper() == "OUTSTANDING").Sum(ss => ss.penaltyAmount);
            var sumPenaltyAmountSchedule = _context.TR_PenaltySchedule.Where(ss => ss.bookingHeaderID == bookingHeaderID).Sum(ss => ss.penaltyAmount);

            decimal Net = decimal.Parse(1.1.ToString());
            decimal Vat = decimal.Parse(0.1.ToString());
            var t = 1;

            var dueDate = DateTime.Now;
            for (int i = 0; i < NetPrice.Count; i++)
            {
               
                var persentase = NetPrice[i].netNetPrice / sumNetPrice;
                            
                try
                {
             
                    var idBookingDetail = NetPrice[i].idBookingDetail;

                    var query = "select a.Id BookingDetailIdSchedule, a.schedNo Schedno from TR_BookingDetailSchedule a" +
                                    " inner join LK_Alloc b on a.allocID = b.Id" +
                                    " where bookingDetailID = @bookingDetailID and b.allocCode = 'PEN'" +
                                    " order by schedNo desc";
                    var values = new { bookingDetailID = idBookingDetail };
                    var schedule = _getStoredProcedure.SelectQuery<UpdateScheduleDto>(query, values);
                    //var sch = schedule.FirstOrDefault().Schedno + t;
                    var sch = _context.TR_BookingDetailSchedule.Where(ss => ss.bookingDetailID == NetPrice[i].idBookingDetail && ss.LK_Alloc.allocCode != "PEN").OrderByDescending(ss => ss.schedNo).FirstOrDefault().schedNo + Convert.ToInt16(t);

                    if (schedule.Count != 0)
                    {

                        var ID = schedule.FirstOrDefault().BookingDetailIdSchedule;

                        TR_BookingDetailSchedule toUpdate = _context.TR_BookingDetailSchedule.Find(ID);
                        toUpdate.netAmt = (sumPenaltyAmountSchedule / Net) * persentase;
                        toUpdate.netOut = (sumPenaltyAmount / Net) * persentase;
                        toUpdate.vatAmt = ((sumPenaltyAmountSchedule / Net) * Vat) * persentase;
                        toUpdate.vatOut = ((sumPenaltyAmount / Net) * Vat) * persentase;
                        toUpdate.LastModificationTime = DateTime.Now;
                        toUpdate.LastModifierUserId = userID;
                        toUpdate.schedNo = Convert.ToInt16(sch);

                        _context.TR_BookingDetailSchedule.Attach(toUpdate);
                        _context.Entry(toUpdate).State = EntityState.Modified;

                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        int alloc = _context.LK_Alloc.Where(ss => ss.allocCode == "PEN").FirstOrDefault().Id;

                        TR_BookingDetailSchedule toInsert = new TR_BookingDetailSchedule();
                        toInsert.CreationTime = DateTime.Now;
                        toInsert.CreatorUserId = userID;
                        toInsert.allocID = alloc;
                        toInsert.bookingDetailID = NetPrice[i].idBookingDetail;
                        toInsert.dueDate = dueDate;
                        toInsert.entityID = 1;
                        toInsert.remarks = "Update " + DateTime.Now;
                        toInsert.schedNo = Convert.ToInt16(sch);
                        toInsert.netAmt = (sumPenaltyAmountSchedule / Net) * persentase;
                        toInsert.netOut = (sumPenaltyAmount / Net) * persentase;
                        toInsert.vatAmt = ((sumPenaltyAmountSchedule / Net) * Vat) * persentase;
                        toInsert.vatOut = ((sumPenaltyAmount / Net) * Vat) * persentase;

                        _context.TR_BookingDetailSchedule.Attach(toInsert);
                        _context.Entry(toInsert).State = EntityState.Added;

                        _context.TR_BookingDetailSchedule.Add(toInsert);
                        await _context.SaveChangesAsync();

                        flag = true;
                    }
                }
                catch (Exception e)
                {
                    flag = false;
                    throw;
                }


            }

            return flag;
            //try
            //{
            //    if (cekExistingDataBookingSchedule != null)
            //    {
            //        int ID = cekExistingDataBookingSchedule.Id;
            //        TR_BookingDetailSchedule toUpdate = _contextPropertySystem.TR_BookingDetailSchedule.Find(ID);
            //        toUpdate.netAmt = sumPenaltyAmount / Net;
            //        toUpdate.netOut = sumPenaltyAmount / Net;
            //        toUpdate.vatAmt = (sumPenaltyAmount / Net) * Vat;
            //        toUpdate.vatOut = (sumPenaltyAmount / Net) * Vat;
            //        toUpdate.LastModificationTime = DateTime.Now;
            //        toUpdate.LastModifierUserId = userID;
            //        toUpdate.schedNo = Convert.ToInt16(sch);

            //        _contextPropertySystem.SaveChanges();
            //    }
            //    else
            //    {
            //        int alloc = _contextPropertySystem.LK_Alloc.Where(ss => ss.allocCode == "PEN").FirstOrDefault().Id;                    

            //        TR_BookingDetailSchedule toInsert = new TR_BookingDetailSchedule();
            //        toInsert.CreationTime = DateTime.Now;
            //        toInsert.CreatorUserId = userID;
            //        toInsert.allocID = alloc;
            //        toInsert.bookingDetailID = bookingDetailID;
            //        toInsert.dueDate = DateTime.Now;
            //        toInsert.entityID = 1;
            //        toInsert.remarks = "Update " + DateTime.Now;
            //        toInsert.schedNo = Convert.ToInt16(sch);
            //        toInsert.netAmt = sumPenaltyAmount / Net;
            //        toInsert.netOut = sumPenaltyAmount / Net;
            //        toInsert.vatAmt = (sumPenaltyAmount / Net) * Vat;
            //        toInsert.vatOut = (sumPenaltyAmount / Net) * Vat;

            //        _contextPropertySystem.TR_BookingDetailSchedule.Add(toInsert);
            //        _contextPropertySystem.SaveChanges();
            //    }
            //}
            //catch (Exception)
            //{

            //    throw;
            //}

        }

        private async Task<bool> WaivePenalty(int bookingHeaderID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;
                var procedure = "[SP_CekWaivePenaltyPayment]";
                var values = new { bookingHeaderID = bookingHeaderID };

                try
                {
                    var status = _getStoredProcedure.ExecSP<int>(procedure, values);

                    //Logger.DebugFormat("WaivePenalty() - Ended WaivePenalty");
                    flag = true;
                }
                catch (Exception)
                {
                    flag = false;
                    //Logger.DebugFormat("WaivePenalty() - Wrong Description");
                }

                return flag;
            });

        }

        private string FailMessage(bool flagBulkUpadate, bool flagBulkInsert, bool flagRefreshPenalty, bool flagWaivePenalty, bool flagUpdateBookingSchedule)
        {
            string msg = "";

            if (flagBulkUpadate)
            {
                msg = "Error Process BulkUpadate";
            }
            else if (flagBulkInsert)
            {
                msg = "Error Process BulkInsert";
            }
            else if (flagRefreshPenalty)
            {
                msg = "Error Process RefreshPenalty";
            }
            else if (flagWaivePenalty)
            {
                msg = "Error Process WaivePenalty";
            }
            else if (flagUpdateBookingSchedule)
            {
                msg = "Error Process UpdateBookingSchedule";
            }


            return msg;
        }
       
    }
}
