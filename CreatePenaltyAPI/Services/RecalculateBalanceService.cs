﻿using CreatePenaltyAPI.Context;
using CreatePenaltyAPI.Helper.Interface;
using CreatePenaltyAPI.Model;
using CreatePenaltyAPI.Services.Interface;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace CreatePenaltyAPI.Services
{
    public class RecalculateBalanceService : IRecalculateBalanceService
    {
        private readonly STGPropertySystemContext _context;
        private readonly IConfiguration _appConfiguration;
        private readonly IGetStoredProcedure _getStoredProcedure;

        private List<MS_ProjectPPN> _mappingPPN;
        private decimal _valuePPN;

        public RecalculateBalanceService(
                STGPropertySystemContext sTGPropertySystemContext,
                IConfiguration configuration,
                IGetStoredProcedure getStoredProcedure)
        {
            _context = sTGPropertySystemContext;
            _appConfiguration = configuration;
            _getStoredProcedure = getStoredProcedure;
        }

        public async Task<RecalculateBalanceResultDto> RecalculateBalanceManual(List<string> listBookCode)
        {
            RecalculateBalanceResultDto result = new RecalculateBalanceResultDto();
            List<RecalculateBalanceListBookcodeFailDto> listRow = new List<RecalculateBalanceListBookcodeFailDto>();

            DataTable dtPayment = new DataTable();
            ColumnsPaymentHeader(dtPayment);
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);

            int succes = 0;
            int fail = 0;

            try
            {
                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        List<GetPSASParamsBookingScheduleDto> scheduleList = new List<GetPSASParamsBookingScheduleDto>();
                        List<GetPSASParamPaymentDto> paymentList = new List<GetPSASParamPaymentDto>();

                        var userName = "Admin";

                        RecalculateBalanceListBookcodeFailDto temp = new RecalculateBalanceListBookcodeFailDto();

                        try
                        {
                            var projectID = GetProjectID(data);
                            GetValuePPN(projectID);


                            string bookCode = data;

                            RefreshDataAlloc(bookCode);

                            //Get Schedule List
                            var procedure = "[SP_GetBookingSchedule]";
                            var values = new { bookCode = bookCode };

                            var recalcBalanceList = _getStoredProcedure.ExecSP<GetPSASParamsBookingScheduleDto>(procedure, values);
                            scheduleList = recalcBalanceList.ToList();

                            #region payment                            

                            int bookingHeaderID = scheduleList.Count != 0 ? scheduleList.FirstOrDefault().bookingHeaderID : 0;

                            var sp = "[SP_GetPayment]";
                            var param = new { bookingHeaderID = bookingHeaderID, userName = userName };

                            var SP_RefreshDoublePaymentDetail = "[SP_RefreshDoublePaymentDetail]";
                            var paramPaymentDetail = new { bookingHeaderID = bookingHeaderID };

                            var getPaymentList = _getStoredProcedure.ExecSP<GetPSASParamPaymentDto>(sp, param);
                            paymentList = getPaymentList.ToList();

                            var flag = _getStoredProcedure.ExecSP<int>(SP_RefreshDoublePaymentDetail, paramPaymentDetail).FirstOrDefault();

                            #endregion

                            if (paymentList.Count == 0) { continue; }

                            RecalculateBalancePerBookcodeResultDto resultPerBckCode = await DoThis(scheduleList, paymentList);
                            if (resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data, resultPerBckCode.FailMessage);
                            }

                            #region simpan payment temp
                            foreach (var input in paymentList.Where(ss => ss.Amount > 0))
                            {
                                dtPayment.Rows.Add(
                                                    input.PaymentHeaderID,
                                                    input.AmountNet,
                                                    input.AmountVat
                                                );
                            }

                            succes += 1;
                            BulkUpdate(dtPayment);
                            RefreshDataAlloc(bookCode);
                            #endregion
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;

                BulkLOG(dtLOG);
            }
            catch
            {
                
            }

            return result;
        }
        public async Task<RecalculateBalanceResultDto> RecalculateBalanceAllProject() 
        {
            RecalculateBalanceResultDto result = new RecalculateBalanceResultDto();
            List<RecalculateBalanceListBookcodeFailDto> listRow = new List<RecalculateBalanceListBookcodeFailDto>();

            DataTable dtPayment = new DataTable();
            ColumnsPaymentHeader(dtPayment);
            DataTable dtLOG = new DataTable();
            ColumnsLOG(dtLOG);

            int succes = 0;
            int fail = 0;

            try
            {
                var getBookcode = "[SP_GetDataBookCodePerSetup]";

                var parameter = new { flag = "REC" };

                var bookcodes = _getStoredProcedure.ExecSP<string>(getBookcode, parameter);
                var listBookCode = bookcodes.ToList();

                int limit = listBookCode.Count / 5;
                int afterLimit = limit == 0 ? 1 : limit;
                int partition = listBookCode.Count % afterLimit == 0 ? listBookCode.Count / afterLimit : (listBookCode.Count / afterLimit) + 1;

                for (int i = 0; i < partition; i++)
                {
                    var takeData = listBookCode.Skip(afterLimit * i).Take(afterLimit);
                    foreach (var data in takeData)
                    {
                        List<GetPSASParamsBookingScheduleDto> scheduleList = new List<GetPSASParamsBookingScheduleDto>();
                        List<GetPSASParamPaymentDto> paymentList = new List<GetPSASParamPaymentDto>();

                        var userName = "Admin";

                        RecalculateBalanceListBookcodeFailDto temp = new RecalculateBalanceListBookcodeFailDto();

                        try
                        {
                            var projectID = GetProjectID(data);
                            GetValuePPN(projectID);

                            //Get Schedule List
                            string bookCode = data;

                            var procedure = "[SP_GetBookingSchedule]";
                            var values = new { bookCode = bookCode };

                            var recalcBalanceList = _getStoredProcedure.ExecSP<GetPSASParamsBookingScheduleDto>(procedure, values);
                            scheduleList = recalcBalanceList.ToList();

                            #region payment
                            int bookingHeaderID = scheduleList.Count != 0 ? scheduleList.FirstOrDefault().bookingHeaderID : 0;

                            var sp = "[SP_GetPayment]";
                            var param = new { bookingHeaderID = bookingHeaderID, userName = userName };

                            var getPaymentList = _getStoredProcedure.ExecSP<GetPSASParamPaymentDto>(sp, param);
                            paymentList = getPaymentList.ToList();
                            
                            #endregion

                            if (paymentList.Count == 0) { continue; }

                            //sampe sini benerin log buat do this
                            RecalculateBalancePerBookcodeResultDto resultPerBckCode = await DoThis(scheduleList, paymentList);
                            if (resultPerBckCode.FailMessage != "")
                            {
                                dtLOG.Rows.Add(DateTime.Now, data, resultPerBckCode.FailMessage);
                            }

                            #region simpan payment temp
                            foreach (var input in paymentList.Where(ss => ss.Amount > 0))
                            {
                                dtPayment.Rows.Add(
                                                    input.PaymentHeaderID,
                                                    input.AmountNet,
                                                    input.AmountVat
                                                );
                            }

                            succes += 1;
                            BulkUpdate(dtPayment);
                            #endregion
                        }
                        catch (Exception e)
                        {
                            temp.Bookcode = data;
                            temp.FailMessage = e.Message;
                            listRow.Add(temp);

                            dtLOG.Rows.Add(DateTime.Now, data, e.Message);
                            fail += 1;
                            continue;
                            throw;
                        }
                    }
                }

                result.Succes = succes;
                result.Fail = fail;
                result.ListBookcodeFail = listRow;

                BulkLOG(dtLOG);
            }
            catch
            {
                //throw;
            }

            return result;
        }

        private void RefreshDataAlloc(string bookCode)
        {
            var SP_RefreshDataAlloc = "[SP_RefreshDataAlloc]";
            var paramRefreshDataAlloc = new { bookCode = bookCode };

            var status = _getStoredProcedure.ExecSP<int>(SP_RefreshDataAlloc, paramRefreshDataAlloc).FirstOrDefault();
        }

        private void BulkUpdate(DataTable dt)
        {
            string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
            SqlConnection con = new SqlConnection(connection);
            SqlBulkCopy objbulk = new SqlBulkCopy(con);

            using (SqlCommand command = new SqlCommand("", con))
            {
                try
                {
                    con.Open();

                    //Creating temp table on database
                    command.CommandText = "CREATE TABLE #TmpTable(PaymentHeaderID int, AmountNet decimal, AmountVat decimal)";
                    command.ExecuteNonQuery();

                    //Bulk insert into temp table
                    using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con))
                    {
                        bulkcopy.BulkCopyTimeout = 660;
                        bulkcopy.DestinationTableName = "#TmpTable";
                        bulkcopy.WriteToServer(dt);
                        bulkcopy.Close();
                    }

                    // Updating destination table, and dropping temp table
                    //command.CommandTimeout = 300;
                    command.CommandText = ";with cte as (select a.Id,b.PaymentHeaderID,a.amount,b.AmountNet,b.AmountVat from TR_PaymentHeader as a inner join #TmpTable b on a.Id = b.PaymentHeaderID) update cte set amount = (AmountNet + AmountVat); DROP TABLE #TmpTable;";
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    // Handle exception properly
                }
                finally
                {
                    con.Close();
                }
            }
        }

        private void BulkLOG(DataTable dt)
        {
            string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
            SqlConnection con = new SqlConnection(connection);

            using (SqlCommand command = new SqlCommand("", con))
            {
                try
                {
                    con.Open();
                    //Creating temp table on database
                    //command.CommandText = "CREATE TABLE #TmpTable(createdDate datetime, bookCode varchar(max), message_ex varchar(max))";
                    //command.ExecuteNonQuery();

                    ////Bulk insert into temp table
                    //using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con))
                    //{
                    //    bulkcopy.BulkCopyTimeout = 660;
                    //    bulkcopy.DestinationTableName = "#TmpTable";
                    //    bulkcopy.WriteToServer(dt);
                    //    bulkcopy.Close();
                    //}

                    //command.CommandTimeout = 300;
                    //command.CommandText = "DELETE LOG_ProsesRecalculateBalance where bookCode in (select bookCode from #TmpTable)";
                    //command.ExecuteNonQuery();

                    SqlBulkCopy objbulk = new SqlBulkCopy(con);
                    objbulk.DestinationTableName = "LOG_ProsesRecalculateBalance";

                    objbulk.ColumnMappings.Add("createdDate", "createdDate");
                    objbulk.ColumnMappings.Add("bookCode", "bookCode");
                    objbulk.ColumnMappings.Add("message_ex", "message_ex");

                    objbulk.WriteToServer(dt);
                }
                catch (Exception ex)
                {
                    // Handle exception properly
                }
                finally
                {
                    con.Close();
                }
            }
        }

        //ini cikal
        private async Task<RecalculateBalancePerBookcodeResultDto> DoThis(List<GetPSASParamsBookingScheduleDto> scheduleList, List<GetPSASParamPaymentDto> paymentList)
        {
            RecalculateBalancePerBookcodeResultDto result = new RecalculateBalancePerBookcodeResultDto();
            result.FailMessage = "";

            DataTable allocDT = new DataTable();
            DataTable IDPaymentDT = new DataTable();

            ColumnsIDPayment(IDPaymentDT);
            ColumnsAlloc(allocDT);

            int userID = 2;

            ListPaymentDetailAllocTemp tempList = new ListPaymentDetailAllocTemp();
            List<PaymentDetaiAllocTemp> temp = new List<PaymentDetaiAllocTemp>();

            var fillListPayment = paymentList.ToList();

            for (int i = 0; i < fillListPayment.Count; i++)
            {
                try
                {
                    var pay_ = fillListPayment[i];

                    #region Adjust
                    if (pay_.Amount < 0)
                    {
                        int PaymentDetailID = fillListPayment[i].PaymentDetailID;
                        await Adjustment(allocDT, IDPaymentDT, scheduleList, paymentList, tempList, PaymentDetailID, userID);
                        continue;
                    }
                    #endregion

                    SetValuePPN(pay_.Cleardate);

                    for (int j = 0; j < scheduleList.Count; j++)
                    {
                        try
                        {
                            var book_ = scheduleList[j];

                            if (book_.netOut + book_.vatOut != 0)
                            {
                                if (pay_.Amount <= 0)
                                {
                                    break;
                                }

                                decimal amountNet = (pay_.Amount) / (1 + _valuePPN);
                                decimal amountVat = (pay_.Amount) - amountNet;

                                decimal OutStanding = (book_.netOut + book_.vatOut) - Math.Round(pay_.Amount, 4);
                                decimal OutStandingR = OutStanding < 0 ? 0 : OutStanding;

                                decimal sisaAmount = pay_.Amount - (book_.netOut + book_.vatOut);

                                PaymentDetaiAllocTemp model = new PaymentDetaiAllocTemp
                                {
                                    CreationTime = DateTime.Now,
                                    CreatorUserId = userID,
                                    LastModificationTime = DateTime.Now,
                                    LastModifierUserId = userID,
                                    entityID = 1,
                                    netAmt = (OutStandingR == 0) ? book_.netOut : amountNet,
                                    paymentDetailID = fillListPayment[i].PaymentDetailID,
                                    schedNo = book_.schedNo,
                                    vatAmt = (OutStandingR == 0) ? book_.vatOut : amountVat
                                };

                                temp.Add(model);

                                allocDT.Rows.Add(
                                        (j + 1).ToString() + "-" + fillListPayment[i].PaymentDetailID,
                                        DateTime.Now,
                                        userID,
                                        DateTime.Now,
                                        userID,
                                        1,
                                        (OutStandingR == 0) ? book_.netOut : amountNet,
                                        fillListPayment[i].PaymentDetailID,
                                        book_.schedNo,
                                        (OutStandingR == 0) ? book_.vatOut : amountVat
                                    );

                                IDPaymentDT.Rows.Add(fillListPayment[i].PaymentDetailID);

                                if (sisaAmount > 0)
                                {
                                    fillListPayment[i].Amount = sisaAmount;
                                }
                                else
                                {
                                    fillListPayment[i].Amount = 0;
                                }

                                decimal OutNetR = OutStandingR / (1 + _valuePPN);
                                decimal OutVatR = OutStandingR - OutNetR;

                                scheduleList[j].netOut = OutNetR;
                                scheduleList[j].vatOut = OutVatR;

                                var getScheduleList = _context.TR_BookingDetailSchedule.Where(ss => ss.schedNo == scheduleList[j].schedNo &&
                                                            ss.TR_BookingDetail.TR_BookingHeader.Id == scheduleList[j].bookingHeaderID).ToList();
                                var sumNet = getScheduleList.Sum(ss => ss.netOut);
                                var sumVat = getScheduleList.Sum(ss => ss.vatOut);

                                foreach (var updt in getScheduleList)
                                {
                                    int ID = updt.Id;
                                    var persenNet = updt.netOut / sumNet;
                                    var persenVat = book_.pctTax == 0 ? 0 : (updt.vatOut / sumVat);
                                    var OutNetR_ = OutNetR * persenNet;
                                    var OutvatR_ = OutVatR * persenVat;
                                    await BulkUpdateBookingSchedule(ID, OutNetR_, OutvatR_, userID);
                                }
                            }
                        }
                        catch
                        {
                            break;
                        }
                    }

                    #region CekSisa
                    if (pay_.Amount > 0)
                    {
                        decimal RestAmountNet = (pay_.Amount) / (1 + _valuePPN);
                        decimal RestAmountVat = RestAmountNet * _valuePPN;

                        PaymentDetaiAllocTemp allocTemp = new PaymentDetaiAllocTemp
                        {
                            CreationTime = DateTime.Now,
                            CreatorUserId = userID,
                            LastModificationTime = DateTime.Now,
                            LastModifierUserId = userID,
                            entityID = 1,
                            netAmt = Math.Round(RestAmountNet, 4),
                            paymentDetailID = fillListPayment[i].PaymentDetailID,
                            schedNo = 0,
                            vatAmt = Math.Round(RestAmountVat, 4)
                        };

                        temp.Add(allocTemp);

                        allocDT.Rows.Add(
                                       "0-" + fillListPayment[i].PaymentDetailID,
                                       DateTime.Now,
                                       userID,
                                       DateTime.Now,
                                       userID,
                                       1,
                                       Math.Round(RestAmountNet, 4),
                                       fillListPayment[i].PaymentDetailID,
                                       0,
                                       Math.Round(RestAmountVat, 4)
                                   );

                        IDPaymentDT.Rows.Add(fillListPayment[i].PaymentDetailID);
                    }
                    #endregion

                    tempList.ListPaymentDetailAlloc = temp;
                }
                catch
                {
                    break;
                    throw;
                }
            }

            await BulkDeleteAlloc(IDPaymentDT);
            RemoveDuplicateRows(allocDT, "ID");
            await BulkInsert(allocDT);

            return result;
        }

        private async Task Adjustment(DataTable allocDT, DataTable IDPaymentDT, List<GetPSASParamsBookingScheduleDto> scheduleList, List<GetPSASParamPaymentDto> paymentList, ListPaymentDetailAllocTemp Alloc, int PaymentDetailID, int userID)
        {
            var adj = paymentList.Where(ss => ss.isAdj == false && ss.PaymentDetailID == PaymentDetailID).OrderBy(ss => ss.Cleardate).FirstOrDefault();
            var endPay = paymentList.Where(ss => ss.AmountNet + ss.AmountVat > 0 && ss.isAdj == false && ss.Cleardate < adj.Cleardate).OrderByDescending(ss => ss.Cleardate).ThenByDescending(ss => ss.CreationTime).FirstOrDefault();
            endPay.isAdj = true;


            var adjAmount = ((adj.AmountNet + adj.AmountVat) * -1);
            var endPayAmount = endPay.AmountVat + endPay.AmountNet;

            var listAlloc = Alloc.ListPaymentDetailAlloc.Where(ss => ss.paymentDetailID == endPay.PaymentDetailID).OrderBy(ss => ss.CreationTime).ToList();

            SetValuePPN(adj.Cleardate);

            decimal Vat;

            Vat = _valuePPN;

            var vatForZeroTax = scheduleList.Where(x => x.vatAmt == 0).Any();

            if (vatForZeroTax)
            {
                Vat = decimal.Parse(0.0.ToString());
            }

            if (adjAmount != endPayAmount)
            {
                var listAlloc0 = listAlloc.Where(ss => ss.schedNo == 0).FirstOrDefault();
                if (listAlloc0 != null)
                {
                    allocDT.Rows.Add(
                       "Rf-" + "0" + "-" + adj.PaymentDetailID,
                       DateTime.Now,
                       userID,
                       DateTime.Now,
                       userID,
                       1,
                       adj.AmountNet,
                       adj.PaymentDetailID,
                       listAlloc0.schedNo,
                       adj.AmountVat
                   );

                    IDPaymentDT.Rows.Add(adj.PaymentDetailID);
                }
            }
            else
            {
                for (int i = 0; i < listAlloc.Count; i++)
                {
                    try
                    {
                        if (listAlloc[i].schedNo != 0)
                        {
                            var Schedule = scheduleList.Where(ss => ss.schedNo == listAlloc[i].schedNo).FirstOrDefault();
                            Schedule.netOut = Schedule.netOut + listAlloc[i].netAmt;
                            Schedule.vatOut = Schedule.vatOut + listAlloc[i].vatAmt;

                            var getScheduleList = _context.TR_BookingDetailSchedule.Where(ss => ss.schedNo == Schedule.schedNo &&
                                                                ss.TR_BookingDetail.TR_BookingHeader.Id == Schedule.bookingHeaderID).ToList();
                            var sumNet = getScheduleList.Sum(ss => ss.netOut);
                            var sumVat = getScheduleList.Sum(ss => ss.vatOut);

                            foreach (var updt in getScheduleList)
                            {
                                int ID = updt.Id;
                                var persenNet = updt.netOut / sumNet;
                                var persenVat = Vat == 0 ? 0 : (updt.vatOut / sumVat);
                                decimal OutNetR_ = Schedule.netOut * persenNet;
                                decimal OutvatR_ = Schedule.vatOut * persenVat;
                                await BulkUpdateBookingSchedule(ID, OutNetR_, OutvatR_, userID);
                            }
                        }

                        allocDT.Rows.Add(
                            "Adj-" + (i + 1).ToString() + "-" + adj.PaymentDetailID,
                            DateTime.Now,
                            userID,
                            DateTime.Now,
                            userID,
                            1,
                            listAlloc[i].netAmt * -1,
                            adj.PaymentDetailID,
                            listAlloc[i].schedNo,
                            listAlloc[i].vatAmt * -1
                        );

                        IDPaymentDT.Rows.Add(adj.PaymentDetailID);
                    }
                    catch
                    {
                        throw;
                    }

                }
            }

            await BulkDeleteAlloc(IDPaymentDT);
        }
        private async Task<bool> BulkUpdateBookingSchedule(int Id, decimal netOut, decimal vatOut, int userID)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;

                var date = DateTime.Now;
                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
                SqlConnection con = new SqlConnection(connection);
                SqlBulkCopy objbulk = new SqlBulkCopy(con);

                using (SqlCommand command = new SqlCommand("", con))
                {
                    try
                    {
                        con.Open();

                        command.CommandText = "Update TR_BookingDetailSchedule Set netOut =" + netOut + ", vatOut =" + vatOut + ", LastModificationTime='" + date + "', LastModifierUserId=" + userID + " where Id=" + Id;
                        command.CommandTimeout = 120;
                        command.ExecuteNonQuery();
                    }
                    catch
                    {
                        flag = false;
                    }
                    finally
                    {
                        flag = true;
                        con.Close();
                    }
                }

                return flag;
            });            
        }
        private async Task<bool> BulkDeleteAlloc(DataTable dt)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;

                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
                SqlConnection con = new SqlConnection(connection);
                SqlBulkCopy objbulk = new SqlBulkCopy(con);

                using (SqlCommand command = new SqlCommand("", con))
                {
                    try
                    {
                        con.Open();

                        //Creating temp table on database
                        command.CommandText = "CREATE TABLE #TmpTable(paymentDetailID int)";
                        command.ExecuteNonQuery();

                        //Bulk insert into temp table
                        using (SqlBulkCopy bulkcopy = new SqlBulkCopy(con))
                        {
                            //bulkcopy.BulkCopyTimeout = 660;
                            bulkcopy.DestinationTableName = "#TmpTable";
                            bulkcopy.WriteToServer(dt);
                            bulkcopy.Close();
                        }

                        // Updating destination table, and dropping temp table
                        command.CommandTimeout = 300;
                        command.CommandText = "DELETE FROM TR_PaymentDetailAlloc WHERE paymentDetailID IN (SELECT paymentDetailID FROM #TmpTable); DROP TABLE #TmpTable;";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        flag = false;
                        // Handle exception properly
                    }
                    finally
                    {
                        flag = true;
                        con.Close();
                    }
                }

                return flag;
            });            
        }
        private async Task<bool> BulkInsert(DataTable dt)
        {
            return await Task.Factory.StartNew(() =>
            {
                bool flag = false;

                string connection = _appConfiguration.GetValue<string>("ConnectionString:DB");
                //string connection = "Persist Security Info=True;Data Source=10.12.1.43; Initial Catalog=STGPropertySystem; User ID=sa;Password=Password1!";
                SqlConnection con = new SqlConnection(connection);
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                //create object of SqlBulkCopy which help to insert  
                SqlBulkCopy objbulk = new SqlBulkCopy(con);
                try
                {
                    //assign Destination table name  
                    objbulk.DestinationTableName = "TR_PaymentDetailAlloc";
                    objbulk.ColumnMappings.Add("CreationTime", "CreationTime");
                    objbulk.ColumnMappings.Add("CreatorUserId", "CreatorUserId");
                    objbulk.ColumnMappings.Add("LastModificationTime", "LastModificationTime");
                    objbulk.ColumnMappings.Add("LastModifierUserId", "LastModifierUserId");
                    objbulk.ColumnMappings.Add("entityID", "entityID");
                    objbulk.ColumnMappings.Add("netAmt", "netAmt");
                    objbulk.ColumnMappings.Add("paymentDetailID", "paymentDetailID");
                    objbulk.ColumnMappings.Add("schedNo", "schedNo");
                    objbulk.ColumnMappings.Add("vatAmt", "vatAmt");

                    con.Open();
                    //insert bulk Records into DataBase.  
                    objbulk.WriteToServer(dt);
                }
                catch
                {

                }
                finally
                {
                    con.Close();
                }

                return flag;
            });            
        }
        private DataTable ColumnsPaymentHeader(DataTable dt)
        {
            dt.Columns.Add("PaymentHeaderID", typeof(int));
            dt.Columns.Add("AmountNet", typeof(decimal));
            dt.Columns.Add("AmountVat", typeof(decimal));

            return dt;
        }
        private DataTable ColumnsLOG(DataTable dt)
        {
            dt.Columns.Add("createdDate", typeof(DateTime));
            dt.Columns.Add("bookCode", typeof(string));
            dt.Columns.Add("message_ex", typeof(string));

            return dt;
        }
        private DataTable ColumnsIDPayment(DataTable dt)
        {
            dt.Columns.Add("paymentDetailID", typeof(int));

            return dt;
        }
        private DataTable ColumnsAlloc(DataTable dt)
        {
            dt.Columns.Add("ID", typeof(string));
            dt.Columns.Add("CreationTime", typeof(DateTime));
            dt.Columns.Add("CreatorUserId", typeof(long));
            dt.Columns.Add("LastModificationTime", typeof(DateTime));
            dt.Columns.Add("LastModifierUserId", typeof(long));
            dt.Columns.Add("entityID", typeof(int));
            dt.Columns.Add("netAmt", typeof(decimal));
            dt.Columns.Add("paymentDetailID", typeof(int));
            dt.Columns.Add("schedNo", typeof(int));
            dt.Columns.Add("vatAmt", typeof(decimal));

            return dt;
        }
        private DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }

        private void SetValuePPN(DateTime dueDate)
        {
            double getPPN = (from a in _mappingPPN
                             where a.startDate.Value.Date <= dueDate.Date
                             orderby a.startDate descending
                             select a.ppnValue).FirstOrDefault();

            _valuePPN = Convert.ToDecimal(getPPN);
        }

        private void GetValuePPN(int projectID)
        {
            _mappingPPN = (from ppn in _context.MS_ProjectPPN
                           where ppn.projectID == projectID
                           select ppn).ToList();
        }

        private int GetProjectID(string bookCode)
        {
            var projectID = (from a in _context.TR_BookingHeader
                             join b in _context.MS_Unit on a.unitID equals b.Id
                             where a.bookCode == bookCode
                             select b.projectID).FirstOrDefault();

            return projectID;
        }

    }
}
